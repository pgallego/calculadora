def sumar(n1, n2):
    result = n1 + n2
    return result


def restar(n1, n2):
    result = n1 - n2
    return result


def main():
    print(f"1 + 2 = {sumar(1, 2)}")
    print(f"3 + 4 = {sumar(3, 4)}")
    print(f"5 - 6 = {restar(5, 6)}")
    print(f"7 - 8 = {restar(7, 8)}")


if __name__ == "__main__":
    main()
    